<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->kaki . "<br>"; 
echo "Cold blooded : " . $sheep->darah_dingin . "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->kaki . "<br>"; 
echo "Cold blooded : " . $kodok->darah_dingin . "<br>";
$kodok->jump(); 
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->kaki . "<br>"; 
echo "Cold blooded : " . $sungokong->darah_dingin . "<br>";
$sungokong->yell();

?> 